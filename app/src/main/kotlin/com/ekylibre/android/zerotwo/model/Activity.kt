package com.ekylibre.android.zerotwo.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Activity(

    @PrimaryKey
    var id: Long = 0,
    var name: String = "",
    var specie: Specie? = null,
    var account: Account? = null

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}