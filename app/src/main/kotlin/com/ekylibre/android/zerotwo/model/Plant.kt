package com.ekylibre.android.zerotwo.model

import com.squareup.moshi.Json
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import java.util.*


open class Plant(

    @PrimaryKey
    @field:Json(name = "id")
    var id: Long = 0,

    @field:Json(name = "uuid")
    var uuid: String = "",

    @field:Json(name = "name")
    var name: String = "",

    @field:Json(name = "number")
    var number: String = "",

    @field:Json(name = "born_at")
    var born_at: Date = Date(),

    var activity: Activity? = null,

    var account: Account? = null,

    // Only for API
    @Ignore
    @field:Json(name = "activity_id")
    var activity_id: Long? = null,

    @Ignore
    @field:Json(name = "variety")
    var variety: String = ""

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}