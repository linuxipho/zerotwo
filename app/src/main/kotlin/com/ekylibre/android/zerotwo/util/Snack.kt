package com.ekylibre.android.zerotwo.util

import android.view.View
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.ekylibre.android.zerotwo.App
import com.ekylibre.android.zerotwo.R
import com.google.android.material.snackbar.Snackbar
import org.jetbrains.anko.backgroundDrawable
import org.jetbrains.anko.textColor


class Snack {

    companion object {
        fun show(view: View, string: Int, background: Int = 0) {

            val ctx = App.instance

//            val viewGroup = (ctx.findViewById(android.R.id.content) as ViewGroup).getChildAt(0) as ViewGroup

            val snack = Snackbar.make(view, ctx.getString(string), Snackbar.LENGTH_LONG)
            val text = snack.view.findViewById(R.id.snackbar_text) as TextView
            val color = when (background) {
                R.drawable.snackbar_background_success -> R.color.successText
                R.drawable.snackbar_background_error -> R.color.errorText
                else -> R.color.secondaryTextColor
            }
            text.textColor = ContextCompat.getColor(ctx, color)

            if (background != 0)
                snack.view.backgroundDrawable = ContextCompat.getDrawable(ctx, background)
            snack.show()
        }
    }
}