package com.ekylibre.android.zerotwo.ui.inventory.continuous

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.BuildingDivision
import com.ekylibre.android.zerotwo.ui.inventory.continuous.fragments.MainInventoryFragment
import com.ekylibre.android.zerotwo.ui.inventory.continuous.fragments.ProductInventoryFragment
import com.ekylibre.android.zerotwo.ui.inventory.continuous.fragments.SelectZoneFragment
import com.ekylibre.android.zerotwo.util.FragmentNavigator
import org.jetbrains.anko.AnkoLogger


class ContinuousInventoryActivity : AppCompatActivity(), AnkoLogger,
    SelectZoneFragment.OnZoneSelectedListener , MainInventoryFragment.OnZoneSelectedListener {

    companion object {
        enum class Tag {
            MAIN_INVENTORY,
            SELECT_ZONE,
            ZONE_INVENTORY
        }
    }

    private var state: Bundle? = null
    private var currentFragment: Tag = Tag.MAIN_INVENTORY

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inventory_continuous)
        title = "Zones inventoriées"
        state = savedInstanceState

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        replaceFragment(currentFragment)
    }

    override fun onZoneSelected(selectedZone: BuildingDivision) {
        title = selectedZone.name
        replaceFragment(Tag.ZONE_INVENTORY)
    }

    private fun replaceFragment(tag: Tag) {
        currentFragment = tag
        val navigator = FragmentNavigator(R.id.fragment_container, supportFragmentManager)
        state ?: when(tag) {
            Tag.SELECT_ZONE -> navigator.show<SelectZoneFragment>()
            Tag.ZONE_INVENTORY -> navigator.show<ProductInventoryFragment>()
            Tag.MAIN_INVENTORY -> navigator.show<MainInventoryFragment>()
        }
    }

    override fun onBackPressed() {
        navigationRouter()
    }

    override fun onSupportNavigateUp(): Boolean {
        navigationRouter()
        return true
    }

    private fun navigationRouter() = when(currentFragment) {
        Tag.ZONE_INVENTORY -> replaceFragment(Tag.SELECT_ZONE)
        Tag.SELECT_ZONE -> replaceFragment(Tag.MAIN_INVENTORY)
        Tag.MAIN_INVENTORY -> finish()
    }
}