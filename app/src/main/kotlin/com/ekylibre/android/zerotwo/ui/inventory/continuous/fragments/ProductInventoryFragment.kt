package com.ekylibre.android.zerotwo.ui.inventory.continuous.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.Product
import com.ekylibre.android.zerotwo.ui.inventory.continuous.adapters.ProductAdapter
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.frag_continuous_inventory_select_zone.*
import org.jetbrains.anko.AnkoLogger


class ProductInventoryFragment : Fragment(), AnkoLogger {

    private var products: List<Product> = ArrayList()
    private var adapter: ProductAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_continuous_inventory_select_zone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler.layoutManager = LinearLayoutManager(context)
        adapter = ProductAdapter(products)
        recycler.adapter = adapter

        Realm.getDefaultInstance().use { realm ->
            products = realm.where<Product>().findAll()
            adapter?.update(products)
        }

        speedDial.hide()
    }

    companion object {
        @JvmStatic
        fun newInstance() = ProductInventoryFragment()
    }
}
