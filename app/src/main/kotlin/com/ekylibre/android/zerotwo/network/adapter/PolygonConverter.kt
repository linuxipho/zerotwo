package com.ekylibre.android.zerotwo.network.adapter


import com.squareup.moshi.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info


class PolygonConverter: AnkoLogger {

    @FromJson
    fun fromJson(string: String): GeoJsonFeature {

//        val polygons = string.substring(3, string.length - 3)
//            .split("]],\\[\\[".toRegex()).dropLastWhile { it.isEmpty() }
//                .toTypedArray()
//
//        // Use only outer Polygon
//        val points = polygons[0].split("],\\[".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//
//        val lngLats = ArrayList<Point>()
//        for (point in points) {
//            val lngLat = point.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
//            lngLats.add(
//                Point.fromLngLat(
//                    java.lang.Double.parseDouble(lngLat[0]),
//                    java.lang.Double.parseDouble(lngLat[1])
//                )
//            )
//        }
//        return Polygon.fromLngLats(listOf<List<Point>>(lngLats))

        info(string)

        return GeoJsonFeature(string)
    }

    @ToJson
    fun toJson(string: String) = string

}