package com.ekylibre.android.zerotwo.ui

import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.ui.adapter.InterventionsAdapter
import com.ekylibre.android.zerotwo.model.Account
import com.ekylibre.android.zerotwo.model.Intervention
import com.ekylibre.android.zerotwo.util.Preferences
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.layout_main.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.FragmentTransaction
import com.ekylibre.android.zerotwo.App
import com.ekylibre.android.zerotwo.network.EkylibreAPI
import com.ekylibre.android.zerotwo.network.Sync
import com.ekylibre.android.zerotwo.ui.fragment.FilterFragment
import com.ekylibre.android.zerotwo.ui.inventory.continuous.ContinuousInventoryActivity
import com.ekylibre.android.zerotwo.util.Network
import io.realm.RealmResults
import io.realm.Sort
import org.jetbrains.anko.*


class MainActivity : AppCompatActivity(), AnkoLogger,
    NavigationView.OnNavigationItemSelectedListener,
    FilterFragment.OnFragmentInteractionListener {

    companion object {
        //    @Inject
        //    lateinit var api: EkylibreAPI
        enum class Tag {
            INTERVENTION,
            INVENTORY
        }
    }

    private var account: Account = Preferences.account
    private var syncService: Sync = Preferences.syncService
    private var api: EkylibreAPI = Preferences.api
    private lateinit var realm: Realm


    private var interventionList: List<Intervention> = ArrayList()
    private lateinit var dataset: RealmResults<Intervention>

    private var currentFilter: String? = null
    private var currentFragment: Tag = Tag.INTERVENTION

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        info("onCreate")

        // Check we have a database instance, usefull when restarting app
        if (!this::realm.isInitialized)
            realm = Realm.getDefaultInstance()

//        if (!this::account.isInitialized)
//            account = realm.copyFromRealm(realm.where<Account>().equalTo("active", true).findFirst()!!)
//        else
//            info("Account initialized")
//
//        if (!this::api.isInitialized)
//            api = EkylibreAPI.create(this, account)
//
//        if (!this::syncService.isInitialized)
//            syncService = Sync(account, api)

        setContentView(R.layout.activity_main)
        toolbar.title = "Interventions"
//        toolbar.subtitle = "Sous-titre"
        setSupportActionBar(toolbar)

        // Set the FAB button
        fab.setOnClickListener { view ->
            // TODO : set here the FAB action
            Snackbar.make(view, "A Snackbar message...", Snackbar.LENGTH_LONG).setAction("Action", null).show()
        }
        fab.setColorFilter(Color.WHITE)

        // Set the NavigationDrawer
        val toggle = ActionBarDrawerToggle(
            this, activity_main, toolbar,
            R.string.navigation_drawer_open,
            R.string.navigation_drawer_close
        )
        activity_main.addDrawerListener(toggle)
        toggle.syncState()
        nav_drawer.setNavigationItemSelectedListener(this)
        setUserHeader(nav_drawer)

//        replaceFragmentWith(currentFragment)

//        // Chips listeners
//        filter_fertilizing.setOnCheckedChangeListener { _, isChecked ->
//            currentFilter = if (isChecked) "fertilizing" else null
//            queryInterventions()
//        }

        // Set RecyclerView and associated swipe/refresh listener
        interventions_recycler.setHasFixedSize(true)
        interventions_recycler.layoutManager = LinearLayoutManager(this)
        val interventionsAdapter = InterventionsAdapter(interventionList) { item : Intervention -> itemClicked(item) }
        interventions_recycler.adapter = interventionsAdapter

        dataset = realm.where<Intervention>().sort("started_at", Sort.DESCENDING).findAllAsync()
        dataset.addChangeListener { result ->
            interventionsAdapter.update(result)
            info("Interventions list updated !")
        }
    }

    override fun onResume() {
        super.onResume()
        info("onResume")

        nav_drawer.setCheckedItem(R.id.nav_intervention)

//        // Check we have a database instance, usefull when restarting app
//        if (!this::realm.isInitialized)
//            realm = Realm.getDefaultInstance()
//
//        // Query and fill recycler
//        queryInterventions()
    }

//    private fun replaceFragmentWith(fragmentTag: Int)
//    {
//
//        currentFragment = fragmentTag
//
//        val ft = supportFragmentManager.beginTransaction()
//        var fragment: Fragment
//
//        when(fragmentTag) {
//
//            INTERVENTION_FRAGMENT -> {
//                invalidateOptionsMenu()
//                actionBar?.title = "Choix de l'activité"
//                fragment = FilterFragment()
//            }
//        }
//
//        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
////        ft.setCustomAnimations(R.anim.exit_to_left, R.anim.enter_from_right);
//        ft.replace(R.id.fragment_container, fragment, fragmentTag)
//        ft.addToBackStack(null)
//        ft.commit()
//    }

    private fun itemClicked(item: Intervention) {
    }

//    /**
//     * Query database for filtered interventions list
//     */
//    private fun queryInterventions() = runBlocking {
//        val query = realm.where<Intervention>()
//        interventionList = if (currentFilter != null) {
//            query.equalTo("procedure.name", currentFilter).sort("started_at", Sort.DESCENDING).findAll()
//        } else {
//            query.sort("started_at", Sort.DESCENDING).findAll()
//        }
//        interventionsAdapter.update(interventionList)
//    }

    override fun onFragmentInteraction(uri: Uri) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun rotateIcon(menuItem: MenuItem, start: Boolean) {
        if (start) {
            val rotation = AnimationUtils.loadAnimation(this, R.anim.rotation)
            rotation.repeatCount = Animation.INFINITE
            menuItem.actionView = layoutInflater.inflate(R.layout.custom_imageview_sync, null)
            menuItem.actionView.animation = rotation
        } else {
            menuItem.actionView.clearAnimation()
            invalidateOptionsMenu()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        info("onCreateOptionsMenu")
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_sync -> {
            runSync(item)
            true
        }
        R.id.action_settings -> {
            startActivity<SettingsActivity>()
            true
        }
        R.id.action_filter -> {
            val filterFragment = FilterFragment()
            filterFragment.show(getFragmentTransaction(), "dialog")
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_intervention -> {
//                val plant = realm.where<Plant>().isNotNull("activity").findFirst()
//                info("Plant ${plant?.activity?.specie?.name} ${plant?.activity?.specie?.fra}")
            }
            R.id.nav_inventory -> {
                intent = Intent(App.instance, ContinuousInventoryActivity::class.java)
                startActivity(intent)
            }
        }
        activity_main.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onBackPressed() {
        if (activity_main.isDrawerOpen(GravityCompat.START)) {
            activity_main.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.close()
    }

    private fun setUserHeader(nav: NavigationView) {
        // Set navigation header according to current account
        val headerView = nav.inflateHeaderView(R.layout.nav_header_main)
        val headerTitle = headerView.findViewById(R.id.header_title) as TextView
        val headerSubtitle = headerView.findViewById(R.id.header_subtitle) as TextView
        headerTitle.text = account.name
        headerSubtitle.text = account.farm
        // TODO : display user avatar and optionally a custom background
        // val userAvatar = headerView.findViewById(R.id.user_avatar) as ImageView
        // Set navigation header background image
        // doAsync {
        //     val bitmap = Picasso.get().load("file:///android_asset/wheat.jpeg").get()
        //     headerView.background = BitmapDrawable(resources, bitmap)
        // }
    }

    private fun getFragmentTransaction(): FragmentTransaction {
        // DialogFragment.show() will take care of adding the fragment
        // in a transaction.  We also want to remove any currently showing
        // dialog, so make our own transaction and take care of that here.
        val ft = supportFragmentManager.beginTransaction()
        val prev = supportFragmentManager.findFragmentByTag("dialog")
        if (prev != null)
            ft.remove(prev)
        ft.addToBackStack(null)
        return ft
    }

    private fun runSync(item: MenuItem) {
        if (Network.isAvailable(this)) {
            doAsync {

                uiThread {
                    rotateIcon(item, true)
                }

                val message = syncService.getAll()

                uiThread {
                    rotateIcon(item, false)
                    // Colored SnackBar (Bootstrap like)
                    // Snack.show(main_content, R.string.sync_successful, R.drawable.snackbar_background_success)
                    Snackbar.make(main_content, getString(message), Snackbar.LENGTH_LONG).show()
//                    dataset.load()
//                    queryInterventions()
                }
            }
        } else {
            // Colored SnackBar (Bootstrap like)
            //Snack.show(main_content, R.string.no_internet, R.drawable.snackbar_background_error)
            Snackbar.make(main_content, getString(R.string.no_internet), Snackbar.LENGTH_LONG).show()
        }
    }
}
