//package com.ekylibre.android.zerotwo.api
//
//import android.app.IntentService
//import android.content.Intent
//import android.content.Context
//import com.ekylibre.android.zerotwo.model.*
//import com.ekylibre.android.zerotwo.util.Preferences
//import io.realm.Realm
//import io.realm.kotlin.where
//import kotlinx.coroutines.runBlocking
//import org.jetbrains.anko.AnkoLogger
//import org.jetbrains.anko.info
//import org.jetbrains.anko.toast
//
//
//const val GET_ALL = "com.ekylibre.android.zerotwo.action.GET_ALL"
//
////private const val ACTION_BAZ = "com.ekylibre.android.zerotwo.action.BAZ"
////const val EXTRA_PARAM1 = "com.ekylibre.android.zerotwo.extra.PARAM1"
////const val EXTRA_PARAM2 = "com.ekylibre.android.zerotwo.extra.PARAM2"
//
///**
// * An [IntentService] subclass for handling asynchronous task requests in
// * a service on a separate handler thread.
// */
//class SyncService : IntentService("SyncService"), AnkoLogger {
//
//    private lateinit var account: Account
//    private lateinit var api: EkylibreAPI
//
//    override fun onHandleIntent(intent: Intent) {
//
//        account = Preferences.account
//        api = EkylibreAPI.create(this)
//
//        when (intent.action) {
//            GET_ALL -> {
//                getPlants()
//                getInterventions()
//            }
//
////            ACTION_BAZ -> {
////                val param1 = intent.getStringExtra(EXTRA_PARAM1)
////                val param2 = intent.getStringExtra(EXTRA_PARAM2)
////                handleActionBaz(param1, param2)
////            }
//        }
//    }
//
//    /**
//     * Handle action in the provided background thread.
//     */
//    private fun getPlants() {
//
//        info("GET --> Plants")
//
//        runBlocking {
//
//            val response = api.getPlants().await()
//
//            if (response.isSuccessful) {
//                for (plant in response.body()!!) {
//
//                    Realm.getDefaultInstance().use { realm ->
//                        val specie = realm.where<Specie>().equalTo("name", plant.variety).findFirst()
//                        realm.executeTransaction {
//                            var activity: Activity? = null
//                            if (plant.activity_id != null) {
//                                activity = Activity(plant.activity_id!!, "", specie, account)
//                                it.insertOrUpdate(activity)
//                            }
//                            val newPlant = Plant(plant.id, plant.uuid, plant.name, plant.number, plant.born_at, activity, account)
//                            it.insertOrUpdate(newPlant)
//                        }
//                    }
//                }
//                info("--> Plants updated !")
//            } else {
//                toast("Error ${response.code()}")
//            }
//        }
//    }
//
//    /**
//     * Handle action in the provided background thread.
//     */
//    private fun getInterventions() {
//
//        info("GET --> Interventions")
//
////        val call = api.getInterventions()
////        val response = call.execute()
////
////        if (response.isSuccessful) {
////            info(response.body())
////            for (inter in response.body()!!) {
////
////                Realm.getDefaultInstance().use { realm ->
////                    realm.executeTransaction {
////                        val newInter = Intervention(inter.id, "", inter.procedure, inter.number, inter.name,
////                            inter.started_at, inter.stopped_at, inter.description, account)
////                        it.copyToRealmOrUpdate(newInter)
////                    }
////                }
////            }
////            info("--> Interventions updated !")
////        } else {
////            toast("Error ${response.code()}")
////        }
//    }
//
////    /**
////     * Handle action Baz in the provided background thread with the provided
////     * parameters.
////     */
////    private fun handleActionBaz(param1: String, param2: String) {
////        TODO("Handle action Baz")
////    }
//
//    companion object {
//
//        @JvmStatic
//        fun getAll(context: Context) {
//            val intent = Intent(context, SyncService::class.java).apply {
//                action = GET_ALL
//            }
//            context.startService(intent)
//        }
//
////        /**
////         * Starts this service to perform action Baz with the given parameters. If
////         * the service is already performing a task this action will be queued.
////         *
////         * @see IntentService
////         */
////        // TODO: Customize helper method
////        @JvmStatic
////        fun startActionBaz(context: Context, param1: String, param2: String) {
////            val intent = Intent(context, SyncService::class.java).apply {
////                action = ACTION_BAZ
////                putExtra(EXTRA_PARAM1, param1)
////                putExtra(EXTRA_PARAM2, param2)
////            }
////            context.startService(intent)
////        }
//    }
//}
