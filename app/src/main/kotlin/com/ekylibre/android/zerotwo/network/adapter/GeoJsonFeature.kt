package com.ekylibre.android.zerotwo.network.adapter

data class GeoJsonFeature (
    var feature: String
)