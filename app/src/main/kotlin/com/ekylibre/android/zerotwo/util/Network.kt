package com.ekylibre.android.zerotwo.util

import android.content.Context
import android.net.NetworkInfo
import android.net.ConnectivityManager


class Network {
    companion object {

        fun isAvailable(context: Context): Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val activeNetwork: NetworkInfo? = connectivityManager.activeNetworkInfo
            return activeNetwork?.isConnected ?: false
        }
    }
}