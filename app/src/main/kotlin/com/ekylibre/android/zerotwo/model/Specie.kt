package com.ekylibre.android.zerotwo.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Specie(

    @PrimaryKey
    var name: String = "",
    var fra: String = ""

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}