package com.ekylibre.android.zerotwo.model

import com.squareup.moshi.Json
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*


open class Intervention(

    @PrimaryKey
    @field:Json(name = "id")
    var id: Long = 0,

    @field:Json(name = "uuid")
    var uuid: String = "",

    @field:Json(name = "procedure_name")
    var procedure: Procedure? = null,

    @field:Json(name = "number")
    var number: Long = 0,

    @field:Json(name = "name")
    var name: String = "",

    @field:Json(name = "started_at")
    var started_at: Date? = null,

    @field:Json(name = "stopped_at")
    var stopped_at: Date? = null,

    @field:Json(name = "description")
    var description: String? = "",

    var account: Account? = null

    // Only for API
//    @Ignore
//    @field:Json(name = "activity_id")
//    var activity_id: Long? = null,
//
//    @Ignore
//    @field:Json(name = "variety")
//    var variety: String = ""

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}