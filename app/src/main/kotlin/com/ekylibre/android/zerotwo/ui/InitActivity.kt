package com.ekylibre.android.zerotwo.ui

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import androidx.appcompat.app.AppCompatActivity
import com.ekylibre.android.zerotwo.network.EkylibreAPI
import com.ekylibre.android.zerotwo.network.Sync
import com.ekylibre.android.zerotwo.model.*
import com.ekylibre.android.zerotwo.util.Preferences
import io.realm.Realm
import io.realm.kotlin.createObject
import io.realm.kotlin.where
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.info
import java.util.*


class InitActivity : AppCompatActivity(), AnkoLogger {

    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Init dependency injection
//        App.appComponent.inject(applicationContext as App)

        // Get Realm database
        realm = Realm.getDefaultInstance()
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Do actions if first run
        val firstRun = sharedPreferences!!.getBoolean("first_run", true)
        if (firstRun) {

            createLocalDevAccount()
            loadInitialData()
            insertDummyBuildingDivisions()

            // Save that first run was executed
            val editor = sharedPreferences.edit()
            editor.putBoolean("first_run", false).apply()
        }

        // Get current account
        Preferences.account = realm.copyFromRealm(realm.where<Account>().equalTo("active", true).findFirst()!!)
        Preferences.api = EkylibreAPI.create(this, Preferences.account)
        Preferences.syncService = Sync(Preferences.account, Preferences.api)

        // Keep the spalsh screen a while !
        //Thread.sleep(500)

        // Start main activity
        val intent = Intent(this, MainActivity::class.java)

        startActivity(intent)
        finish()
    }

    override fun onDestroy() {
        super.onDestroy()
        realm.close()
    }

    /**
     * Load data from CSV assets
     */
    private fun loadInitialData() {

        info("First run --> creating database...")

        var file = "species.json"
        info("First run --> loading $file")
        realm.executeTransaction {
            it.createAllFromJson(Specie::class.java, assets.open(file))
        }

        file = "procedures.json"
        info("First run --> loading $file")
        realm.executeTransaction {
            it.createAllFromJson(Procedure::class.java, assets.open(file))
        }

    }

    /**
     * Dev purpose only
     */
    private fun createDevAccount() {
        realm.executeTransaction {
            val account = it.createObject<Account>("https://sia-sima-2017.ekylibre.farm")
            account.name = "Rémi de Chazelles"
            account.email = "admin@ekylibre.org"
            account.farm = "GAEC du bois joli"
            account.token = "J5wsFqE29kRw37UgzCSd"
            account.active = true
        }
    }

//    private fun createLocalDevAccount() {
//        realm.executeTransaction {
//            val account = it.createObject<Account>("http://192.168.0.122:8080")
//            account.name = "Rémi de Chazelles"
//            account.email = "admin@ekylibre.org"
//            account.farm = "La ferme des licornes"
//            account.token = "sWgTVxbW5j1CTnhPzf8h"
//            account.active = true
//        }
//    }

    private fun createLocalDevAccount() {
        realm.executeTransaction {
            val account = it.createObject<Account>("http://192.168.0.119:8080")
            account.name = "Rémi de Chazelles"
            account.email = "admin@ekylibre.org"
            account.farm = "La ferme des licornes"
            account.token = "QUzmYSczjyFzvX7xtm4h"
            account.active = true
        }
    }

    private fun insertDummyBuildingDivisions() {
        realm.executeTransaction {
//            for (i in 1..20) {
//                val buildingDivision = it.createObject<BuildingDivision>(i)
//                buildingDivision.name = "Zone de stockage n°$i"
//                buildingDivision.shape = "MULTIPOLYGON (((3.727759863249957 43.78419044015834, 3.7278698338195686 43.784157520941775, 3.7278108252212405 43.78406844532369, 3.72770621906966 43.78410523744291, 3.727759863249957 43.78419044015834)))"
//                buildingDivision.last_sync = Date()
//            }

            for (i in 1..20) {
                val item = it.createObject<Product>(i)
                item.name = "Ammonitrate 33% n°$i"
                item.unit = "Sac de 600 kg"
                item.created_at = Date()
            }
        }
    }
}