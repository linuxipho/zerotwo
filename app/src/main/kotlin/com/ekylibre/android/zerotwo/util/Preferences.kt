package com.ekylibre.android.zerotwo.util

import com.ekylibre.android.zerotwo.network.EkylibreAPI
import com.ekylibre.android.zerotwo.network.Sync
import com.ekylibre.android.zerotwo.model.Account

object Preferences {

    lateinit var account: Account
    lateinit var api: EkylibreAPI
    lateinit var syncService: Sync
}