package com.ekylibre.android.zerotwo.network

import android.content.Context
import com.ekylibre.android.zerotwo.network.adapter.ProcedureConverter
import com.ekylibre.android.zerotwo.model.Account
import com.ekylibre.android.zerotwo.model.BuildingDivision
import com.ekylibre.android.zerotwo.model.Intervention
import com.ekylibre.android.zerotwo.model.Plant
import com.ekylibre.android.zerotwo.model.pokos.Token
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.common.GooglePlayServicesRepairableException
import com.google.android.gms.security.ProviderInstaller
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*
import com.squareup.moshi.Rfc3339DateJsonAdapter
import com.squareup.moshi.Moshi
import kotlinx.coroutines.Deferred
import retrofit2.Response
import java.util.*
import java.util.concurrent.TimeUnit


interface EkylibreAPI {

    @FormUrlEncoded
    @POST("/oauth/token")
    fun getNewAccessToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("grant_type") grantType: String,
        @Field("username") username: String,
        @Field("password") password: String,
        @Field("scope") scope: String
    ): Token

    @FormUrlEncoded
    @POST("/oauth/token")
    fun getRefreshAccessToken(
        @Field("client_id") clientId: String,
        @Field("client_secret") clientSecret: String,
        @Field("refresh_token") refreshToken: String,
        @Field("grant_type") grantType: String
    ): Token

    @GET("plants")
    fun getPlantsAsync(): Deferred<Response<List<Plant>>>

    @GET("interventions")
    fun getInterventionsAsync(): Deferred<Response<List<Intervention>>>

    @GET("building_divisions")
    fun getBuildingDivisionsAsync(): Deferred<Response<List<BuildingDivision>>>


    companion object {

        fun create(context: Context, account: Account): EkylibreAPI {

            // Manage SSL handshake errors
            try {
                ProviderInstaller.installIfNeeded(context)
            } catch (e: GooglePlayServicesRepairableException) {
                GoogleApiAvailability.getInstance().showErrorNotification(context, e.connectionStatusCode)
                error("GooglePlayServicesRepairableException")
            } catch (e: GooglePlayServicesNotAvailableException) {
                error("GooglePlayServicesNotAvailableException")
            }

            // Assembles simple token
            val simpleToken = "simple-token ${account.email} ${account.token}"

            // Set header interceptor
            val httpClient = OkHttpClient().newBuilder()
            val interceptor = Interceptor { chain ->
                chain.proceed(chain.request().newBuilder()
                    .addHeader("Authorization", simpleToken)
                    .build())
            }
            httpClient
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .networkInterceptors().add(interceptor)

            // Attach custom types adapter
            val moshi = Moshi.Builder()
                .add<Date>(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
                .add(ProcedureConverter())
//                .add(PolygonConverter())
                .build()

            // Build retrofit client
            val retrofit = Retrofit.Builder()
                .baseUrl("${account.url}/api/v1/")
                .client(httpClient.build())
                .addConverterFactory(MoshiConverterFactory.create(moshi))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .build()

            return retrofit.create(EkylibreAPI::class.java)
        }
    }

}