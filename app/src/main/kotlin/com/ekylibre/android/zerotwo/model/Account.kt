package com.ekylibre.android.zerotwo.model

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*


open class Account(

    var name: String = "",
    var email: String = "",
    var farm: String = "",
    @PrimaryKey
    var url: String = "",
    var token: String = "",
    var active: Boolean = true,
    var last_sync: Date? = null

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}