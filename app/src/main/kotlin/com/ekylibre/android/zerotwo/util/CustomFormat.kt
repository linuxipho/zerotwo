package com.ekylibre.android.zerotwo.util

import android.content.res.Resources
import android.text.format.DateUtils.isToday
import com.ekylibre.android.zerotwo.App
import com.ekylibre.android.zerotwo.R
import java.text.SimpleDateFormat
import java.util.*


class CustomFormat {

    companion object {

        val res: Resources = App.instance.resources

        private fun isYesterday(date: Long): Boolean {
            val now = Calendar.getInstance()
            val cdate = Calendar.getInstance()
            cdate.timeInMillis = date
            now.add(Calendar.DATE, -1)

            return (now.get(Calendar.YEAR) == cdate.get(Calendar.YEAR)
                    && now.get(Calendar.MONTH) == cdate.get(Calendar.MONTH)
                    && now.get(Calendar.DATE) == cdate.get(Calendar.DATE))
        }

        private fun isNotThisYear(date: Long): Boolean {
            val now = Calendar.getInstance()
            val cdate = Calendar.getInstance()
            cdate.timeInMillis = date

            return now.get(Calendar.YEAR) != cdate.get(Calendar.YEAR)
        }

        private fun formatDate(pattern: String, date: Date): String {
            return SimpleDateFormat(pattern, App.locale).format(date)
        }

        /**
         * Return a human readable time
         */
        fun humanTime(date: Date): String {
            val timeString = formatDate("HH:mm", date)
            return when {
                isToday(date.time) -> res.getString(R.string.at_time, timeString)
                isYesterday(date.time) -> res.getString(R.string.yesterday_at_time, timeString)
                else -> res.getString(R.string.someday_at_time, formatDate("d MMM", date), timeString)
            }
        }

        /**
         * Return a simplified human readable string date
         */
        fun humanDate(date: Date): String {
            return when {
                isToday(date.time)-> res.getString(R.string.today)
                isYesterday(date.time) -> res.getString(R.string.yesterday)
                isNotThisYear(date.time) -> formatDate("d MMM YYYY", date)
                else -> formatDate("d MMM", date)
            }
        }
    }
}