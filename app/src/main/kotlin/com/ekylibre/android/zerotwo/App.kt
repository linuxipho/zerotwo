package com.ekylibre.android.zerotwo

import android.app.Application
//import com.ekylibre.android.zerotwo.di.AppComponent
//import com.ekylibre.android.zerotwo.di.AppModule
//import com.ekylibre.android.zerotwo.di.DaggerAppComponent
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.*


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        instance = this
        locale = Locale.getDefault()
//        setupDependencyInjection()
        initRealm()
    }

//    private fun setupDependencyInjection() {
//        appComponent = DaggerAppComponent
//            .builder()
//            .appModule(AppModule(this))
//            .build()
//        appComponent.inject(this)
//    }

    private fun initRealm() {
        Realm.init(this)
        val config = RealmConfiguration.Builder().name("database.realm").build()
        Realm.setDefaultConfiguration(config)
    }

    companion object {
        @JvmStatic lateinit var instance: App private set
        @JvmStatic lateinit var locale: Locale private set
//        @JvmStatic lateinit var appComponent: AppComponent
    }
}