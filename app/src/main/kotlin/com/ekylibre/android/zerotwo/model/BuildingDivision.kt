package com.ekylibre.android.zerotwo.model

import com.ekylibre.android.zerotwo.network.adapter.GeoJsonFeature
import com.ekylibre.android.zerotwo.util.geo.WKT
import com.mapbox.geojson.Polygon
import com.squareup.moshi.Json
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import java.util.*


open class BuildingDivision (

    @PrimaryKey
    @field:Json(name = "id")
    var id: Long = 0,

    @field:Json(name = "name")
    var name: String = "",

    var wkt: String = "",

    @Ignore
    @field:Json(name = "shape")
    var shape: GeoJsonFeature = GeoJsonFeature(""),

    var last_sync: Date? = null

) : RealmObject() {

    fun polygon() : Polygon? {

        // TODO: implement when API will give GeoJSON instead of WKT
        return WKT.toPolygon(wkt)
    }
}