package com.ekylibre.android.zerotwo.ui.inventory.continuous.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.Product
import com.ekylibre.android.zerotwo.util.CustomFormat.Companion.humanDate
import com.ekylibre.android.zerotwo.util.extension.afterTextChanged
import kotlinx.android.synthetic.main.item_inventory_product.view.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.sdk27.coroutines.onCheckedChange


class ProductAdapter(private var dataset : List<Product>)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

//    private var currentProductTimer: Duration = Duration.ofSeconds(3)
    private var lastProductViewReference: View? = null

    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun display(item: Product) = with(itemView) {
            // Set data
            product_name.text = item.name
            product_unit.text = item.unit
            product_last_inventory.text = humanDate(item.created_at)
            quantity_edit.apply { setText("${item.id}") }

//            Picasso.get().load("file:///android_asset/wheat.jpeg").resize(50, 50).centerCrop()
//                .placeholder(R.drawable.ic_menu_gallery).into(product_picture)

            // Set odd and even background
            val backgroundId = if (adapterPosition % 2 == 1) R.color.light_grey else R.color.white
            backgroundColor = ContextCompat.getColor(context, backgroundId)
            button_minus.setOnClickListener {
                lockProductEdition(itemView)
                val quantity = quantity_edit.text.toString().toDouble().minus(1)
                quantity_edit.apply { setText(quantity.toString()) }
            }
            button_plus.setOnClickListener {
                lockProductEdition(itemView)
                val quantity = quantity_edit.text.toString().toDouble().plus(1)
                quantity_edit.apply { setText(quantity.toString()) }
            }

            quantity_edit.afterTextChanged {
                lockProductEdition(itemView)
                val quantity = it.split('.')
                if (quantity.size == 2)
                    if (quantity[1].isNotBlank() && quantity[1].toInt() == 0)
                        quantity_edit.apply { setText(quantity[0]) }
            }

            quantity_edit.setOnFocusChangeListener { v, hasFocus ->
                if (!hasFocus) {
                    quantity_group.visibility = GONE
                    checkbox.visibility = VISIBLE
                    checkbox.isChecked = true
                } else {
                    quantity_group.visibility = VISIBLE
                    checkbox.visibility = GONE
                }
            }

//            quantity_edit.setOnEditorActionListener { v, actionId, event ->
//                return@setOnEditorActionListener if (actionId == EditorInfo.IME_ACTION_DONE) {
//                    lockProductEdition(itemView)
//                    true
//                } else {
//                    true
//                }
//            }

            checkbox.onCheckedChange { buttonView, isChecked ->
                if (!isChecked) {
                    checkbox.visibility = GONE
                    quantity_group.visibility = VISIBLE
                }
            }
        }
    }

    fun lockProductEdition(currentView: View) {
        if (lastProductViewReference != null && lastProductViewReference != currentView) {
            lastProductViewReference!!.quantity_group.visibility = GONE
            lastProductViewReference!!.checkbox.visibility = VISIBLE
            lastProductViewReference!!.checkbox.isChecked = true
        }
        lastProductViewReference = currentView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context)
            .inflate(R.layout.item_inventory_product, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).display(dataset[position])
    }

    override fun getItemCount(): Int = dataset.size

    fun update(newDataset: List<Product>) {
        dataset = newDataset
        notifyDataSetChanged()
    }
}

