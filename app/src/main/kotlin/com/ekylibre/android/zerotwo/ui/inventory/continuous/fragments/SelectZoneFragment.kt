package com.ekylibre.android.zerotwo.ui.inventory.continuous.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.ekylibre.android.zerotwo.App
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.BuildingDivision
import com.ekylibre.android.zerotwo.ui.inventory.continuous.adapters.SelectZoneAdapter
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.android.synthetic.main.frag_continuous_inventory_select_zone.*
import org.jetbrains.anko.AnkoLogger
import com.leinardi.android.speeddial.SpeedDialActionItem


class SelectZoneFragment : Fragment(), AnkoLogger {

    private var listener: OnZoneSelectedListener? = null
    private var buildingDivisions: List<BuildingDivision> = ArrayList()
    private var adapter: SelectZoneAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.frag_continuous_inventory_select_zone, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recycler.layoutManager = LinearLayoutManager(context)
        adapter = SelectZoneAdapter(buildingDivisions) { item: BuildingDivision ->
            listener?.onZoneSelected(item)
        }
        recycler.adapter = adapter

        Realm.getDefaultInstance().use { realm ->
            buildingDivisions = realm.where<BuildingDivision>().findAll()
            adapter?.update(buildingDivisions)
        }

        speedDial.addActionItem(SpeedDialActionItem.Builder(R.id.fab_continuous_inventory, R.drawable.ic_inventory_add)
            .setLabel("Inventaire courrant").create()
        )
        speedDial.addActionItem(SpeedDialActionItem.Builder(R.id.fab_annual_inventory, R.drawable.ic_inventory_add)
            .setFabBackgroundColor(ContextCompat.getColor(App.instance, R.color.grey))
            .setLabel("Inventaire annuel").create()
        )

        speedDial.setOnActionSelectedListener { actionItem ->
            return@setOnActionSelectedListener when (actionItem.id) {
                R.id.fab_continuous_inventory -> {
                    false
                }
                R.id.fab_annual_inventory -> {
                    false
                }
                else -> false
            }
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnZoneSelectedListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnZoneSelectedListener {
        fun onZoneSelected(selectedZone: BuildingDivision)
    }
}
