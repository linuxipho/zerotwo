package com.ekylibre.android.zerotwo.ui.inventory.continuous.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.BuildingDivision
import com.ekylibre.android.zerotwo.util.CustomFormat
import com.mapbox.api.staticmap.v1.MapboxStaticMap
import com.mapbox.api.staticmap.v1.StaticMapCriteria.LIGHT_STYLE
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_inventory_zone.view.*
import org.jetbrains.anko.AnkoLogger
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.info


class SelectZoneAdapter(private var dataset : List<BuildingDivision>, private val clickListener: (BuildingDivision) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>(), AnkoLogger {

    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun display(item: BuildingDivision, clickListener: (BuildingDivision) -> Unit) = with(itemView) {
            // Set data
            item_inventory_zone_name.text = item.name
            item_inventory_zone_last_date.text = item.last_sync?.let { CustomFormat.humanTime(it) } ?: "pas d'inventaire"
            // Set odd and even background
            val backgroundId = if (adapterPosition % 2 == 1) R.color.light_grey else R.color.white
            backgroundColor = ContextCompat.getColor(context, backgroundId)
            // Set click listener
            setOnClickListener { clickListener(item)}

            info("Polygon ${item.polygon()}")

            // TODO: implement when API will give GeoJSON instead of WKT
            val staticImage = MapboxStaticMap.builder()
                .accessToken(context.getString(R.string.mapbox_token))
                .width(128)
                .height(128)
                .retina(true)
                .geoJson(item.polygon())
                .cameraAuto(true)
                .attribution(false)
                .logo(false)
                .styleId(LIGHT_STYLE)
                .build()
            val imageUrl = staticImage.url().toString()
            Picasso.get()
                .load(imageUrl)
                .placeholder(R.drawable.ic_menu_gallery)
                .into(item_inventory_zone_shape)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_inventory_zone, parent, false))
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).display(dataset[position], clickListener)
    }

    override fun getItemCount(): Int = dataset.size

    fun update(newDataset: List<BuildingDivision>) {
        dataset = newDataset
        notifyDataSetChanged()
    }
}

