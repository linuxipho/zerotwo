//package com.ekylibre.android.zerotwo.di
//
//import android.content.Context
//import com.ekylibre.android.zerotwo.App
//import com.ekylibre.android.zerotwo.api.EkylibreAPI
//import dagger.Module
//import dagger.Provides
//import dagger.Reusable
//import javax.inject.Singleton
//
//@Module
//class AppModule(private val app: App) {
//
//    @Provides
//    @Singleton
//    fun provideApplicationContext(): Context = app
//
//    @Provides
//    @Reusable
//    internal fun provideEkylibreAPI(): EkylibreAPI {
//        return EkylibreAPI.create(app)
//    }
//
//}