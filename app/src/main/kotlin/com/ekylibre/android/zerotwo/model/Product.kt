package com.ekylibre.android.zerotwo.model

import com.squareup.moshi.Json
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*


open class Product(

    @PrimaryKey
    @field:Json(name = "id")
    var id: Long = 0,

    @field:Json(name = "name")
    var name: String = "",

    @field:Json(name = "label")
    var label: String = "",

    @field:Json(name = "category_id")
    var category: Category? = null,

    @field:Json(name = "variant_id")
    var variant: Variant? = null,

    @field:Json(name = "description")
    var description: String = "",

    @field:Json(name = "number")
    var number: String = "",

    @field:Json(name = "unit")
    var unit: String = "",

    @field:Json(name = "created_at")
    var created_at: Date = Date()

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}