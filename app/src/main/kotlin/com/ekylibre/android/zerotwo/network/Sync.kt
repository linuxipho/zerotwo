package com.ekylibre.android.zerotwo.network

import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.*
import com.ekylibre.android.zerotwo.util.Preferences
import io.realm.Realm
import io.realm.kotlin.where
import kotlinx.coroutines.*
import org.jetbrains.anko.*
import retrofit2.HttpException
import java.util.*


class Sync(val account: Account, val api: EkylibreAPI): AnkoLogger {

    private fun getPlants() = runBlocking {
        info("GET --> Plants")

        val response = api.getPlantsAsync().await()

        if (response.isSuccessful) {
            for (plant in response.body()!!) {

                Realm.getDefaultInstance().use { realm ->
                    val specie = realm.where<Specie>().equalTo("name", plant.variety).findFirst()
                    realm.executeTransaction {
                        var activity: Activity? = null
                        if (plant.activity_id != null) {
                            activity = Activity(plant.activity_id!!, "", specie, account)
                            it.insertOrUpdate(activity)
                        }
                        val newPlant =
                            Plant(plant.id, plant.uuid, plant.name, plant.number, plant.born_at, activity, account)
                        it.insertOrUpdate(newPlant)
                    }
                }
            }
            info("--> Plants updated !")
        } else {
            error("getPlants() Error ${response.code()}")
        }
    }

    private fun getInterventions() = runBlocking{
        info("GET --> Interventions")
        val response = api.getInterventionsAsync().await()

        if (response.isSuccessful) {
            for (inter in response.body()!!) {

                Realm.getDefaultInstance().use { realm ->
                    realm.executeTransaction {
                        val newInter = Intervention(inter.id, "", inter.procedure, inter.number, inter.name,
                            inter.started_at, inter.stopped_at, inter.description, account)
                        it.copyToRealmOrUpdate(newInter)
                    }
                }
            }
            info("--> Interventions updated !")
        } else {
            error("getInterventions() Error ${response.code()}")
        }
    }

    private fun getBuildingDivisions() = runBlocking {
        info("GET --> BuildingDivisions")
        val response = api.getBuildingDivisionsAsync().await()

        if (response.isSuccessful) {
            for (building in response.body()!!) {

                info("BuildingDivision #${building.id} ${building.name} -> ${building.shape.feature}")

                Realm.getDefaultInstance().use { realm ->
                    realm.executeTransaction {
                        val newBuilding = BuildingDivision(building.id, building.name, building.shape.feature)
                        it.insertOrUpdate(newBuilding)
                    }
                }
            }
            info("--> BuildingDivisions updated !")
        } else {
            error("getBuildingDivisions() Error ${response.code()}")
        }
    }

    private fun writeLastSyncDate() = runBlocking{
        Realm.getDefaultInstance().use { realm ->
            realm.executeTransaction {
                val account = it.where<Account>().equalTo("token", Preferences.account.token).findFirst()
                account!!.last_sync = Date()
                it.copyToRealmOrUpdate(account)
                Preferences.account = realm.copyFromRealm(account)
            }
        }
    }

    fun getAll(): Int {
        var message = R.string.sync_successful
        try {
            info("======== Starting sync ========")
            getPlants()
            getInterventions()
            getBuildingDivisions()
            writeLastSyncDate()
        }
        catch (e: HttpException) { message = getErrorMessage(e.message()) }
        catch (e: Throwable) { message = getErrorMessage(e.localizedMessage) }
        finally { info("======== Sync finished ========") }

        return message
    }

    private fun getErrorMessage(msg: String): Int {
        error("Error $msg")
        return R.string.sync_failure
    }
}