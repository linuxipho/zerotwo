package com.ekylibre.android.zerotwo.util.geo

import com.mapbox.geojson.Point
import com.mapbox.geojson.Polygon


class WKT {

    companion object {

        fun toPolygon(string: String): Polygon? {

            // Removes coordinates informations (always got WGS84)
            val wkt = string.removePrefix("SRID=4326;")

            val stringPoints = ArrayList<String>()
            val lngLats = ArrayList<Point>()

            // If we have a multipolygon (it's the current common case)
            if (wkt.startsWith("MULTIPOLYGON")) {
                // We know we have a multipolygon, so we remove this info
                val multipolygon = wkt.removePrefix("MULTIPOLYGON (").removeSuffix(")")
                // The multipolygon can effectively contain multiple polygons, we keep only the first one
                var firstPolygon = if (multipolygon.contains(")),(("))
                    multipolygon.split(")),((")[0] else multipolygon
                firstPolygon = firstPolygon.removePrefix("((").removeSuffix("))")
                stringPoints.addAll(firstPolygon.split(", "))
            }

            if (stringPoints.isNotEmpty()) {
                for (point in stringPoints) {
                    val xy = point.split(" ")
                    lngLats.add(Point.fromLngLat(xy[0].toDouble(), xy[1].toDouble()))
                }
            }
            return if (lngLats.isNotEmpty())
                Polygon.fromLngLats(listOf<List<Point>>(lngLats)) else null
        }
    }
}