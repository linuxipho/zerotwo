package com.ekylibre.android.zerotwo.model

import com.squareup.moshi.Json
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey


open class Variant(

    @PrimaryKey
    @field:Json(name = "id")
    var id: Long = 0,

    @field:Json(name = "name")
    var name: String = "",

    @field:Json(name = "type")
    var type: Type? = null

) : RealmObject() {
    // The Kotlin compiler generates standard getters and setters.
    // Realm will overload them and code inside them is ignored.
    // So if you prefer you can also just have empty abstract methods.
}