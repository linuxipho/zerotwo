package com.ekylibre.android.zerotwo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ekylibre.android.zerotwo.R
import com.ekylibre.android.zerotwo.model.Intervention
import com.ekylibre.android.zerotwo.util.CustomFormat
import com.ekylibre.android.zerotwo.util.Preferences
import kotlinx.android.synthetic.main.item_intervention.view.*
import org.jetbrains.anko.backgroundColor


class InterventionsAdapter(private var dataset : List<Intervention>, private val clickListener: (Intervention) -> Unit)
    : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class ViewHolder (itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun display(item: Intervention, clickListener: (Intervention) -> Unit) = with(itemView) {
            // Set last sync date if first item
            if (itemViewType == 0) {
                last_sync_date.visibility = View.VISIBLE
                last_sync_date.text = context.getString(R.string.last_sync, CustomFormat.humanTime(Preferences.account.last_sync!!))
//                val itemLayout = findViewById<ConstraintLayout>(R.id.item_intervention_layout)

//                if (itemLayout.findViewById<TextView>(R.id.last_sync_date) == null) {
//
//                    val textView = TextView(this.context)
//                    textView.text = "Dernière synchronisation " + CustomFormat.justTime(Preferences.account.last_sync!!)
//                    textView.id = R.id.last_sync_date
//                    textView.setTextColor(ContextCompat.getColor(this.context, R.color.grey))
//                    itemLayout.addView(textView)
//
//                    val constraintset = ConstraintSet()
//                    constraintset.clone(itemLayout)
//                    constraintset.connect(textView.id, END, itemLayout.id, END)
//                    constraintset.connect(textView.id, BOTTOM, item_intervention_procedure.id, TOP, 16)
//                    constraintset.applyTo(itemLayout)
//                }
            } else {
                last_sync_date.visibility = View.GONE
            }
            // Set data
            item_intervention_procedure.text = item.name
            item_intervention_date.text = item.started_at?.let { CustomFormat.humanDate(it) }
            // Set odd and even background
            val backgroundId = if (adapterPosition % 2 == 1) R.color.light_grey else R.color.white
            backgroundColor = ContextCompat.getColor(context, backgroundId)
            // Set click listener
            setOnClickListener { clickListener(item)}
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
//        val layout = if (viewType == 0) R.layout.item_intervention_first else R.layout.item_intervention
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_intervention, parent, false))

    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).display(dataset[position], clickListener)
    }

    override fun getItemCount(): Int = dataset.size

    override fun getItemViewType(position: Int): Int = if (position == 0) 0 else 1

    fun update(newDataset: List<Intervention>) {
        dataset = newDataset
        notifyDataSetChanged()
    }
}

