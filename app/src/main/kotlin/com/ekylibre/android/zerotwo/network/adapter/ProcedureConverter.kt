package com.ekylibre.android.zerotwo.network.adapter

import com.ekylibre.android.zerotwo.model.Procedure
import com.squareup.moshi.*
import io.realm.Realm
import org.jetbrains.anko.AnkoLogger


class ProcedureConverter: AnkoLogger {

    @FromJson
    fun fromJson(procedure: String): Procedure {
        Realm.getDefaultInstance().use { realm ->
            val response = realm.where(Procedure::class.java).equalTo("name", procedure).findFirst()
            response?.let {
                return realm.copyFromRealm(it)
            }
            throw JsonDataException("Procedure $procedure could not be parsed")
        }
    }

    @ToJson
    fun toJson(procedure: Procedure) = procedure.name

}